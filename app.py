import requests
from prometheus_client.core import CounterMetricFamily, GaugeMetricFamily, REGISTRY
from prometheus_client import start_http_server
import config
import time

class ReadUrl:

        def usage(self, current_pwr):

                return float(current_pwr)/config.max_pwr*100

        def get_usage(self):
                url = config.url
                
                try:
                        r = requests.get(url)
                        as_list = r.text.split('\n')
                        state = [as_list[10],self.usage(as_list[10]),as_list[11]]
                        return state
                except requests.exceptions.RequestException as e:
                        state = [0,0,0]
                        print('Error:' + str(e))
                        return  state


class PvUsageCollector:

        def collect(self):
                pv_metrics = [
                'power',
                'usage'
                ]

                u = ReadUrl().get_usage()
                v = 0

                for pv_metric in pv_metrics:
                        g = GaugeMetricFamily(
                                'pv_curr',
                                pv_metric,
                                labels = pv_metric[0]
                        )

                        g.add_metric(pv_metric,u[v])
                        v+=1

                        yield g


                c = CounterMetricFamily('pv_today_total', 'total_today',labels = 't')
                c.add_metric('total',u[2])
                yield c

if __name__ == "__main__":
        start_http_server(8000)
        p = PvUsageCollector()
        REGISTRY.register(p)
        
        while True:
                time.sleep(int(config.interval))
